extern crate serde_derive;
extern crate json5;
#[macro_use]
pub mod vector3;
#[macro_use]
pub mod vector4;
#[macro_use]
pub mod quaternion;
pub mod matrix;
