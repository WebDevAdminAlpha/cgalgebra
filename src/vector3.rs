use std::ops;
use std::fmt;
use std::f32;
use matrix::Matrix;
use serde_derive::{Serialize,Deserialize};

#[derive(Copy, Clone,Debug,Serialize,Deserialize)]
pub struct Vec3{
  pub x:f32,
  pub y:f32,
  pub z:f32
}
#[macro_export]
macro_rules! vec3 {
   ($x:expr,$y:expr,$z:expr) => {
      Vec3{x:$x,y:$y,z:$z}      
  };
}

impl fmt::Display for Vec3 {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "x: {}, y: {}, z: {}", self.x, self.y, self.z)
    }
}

impl ops::Add for Vec3{
    type Output = Vec3;
    fn add(self,other:Vec3)->Vec3{
        vec3!(
          self.x + other.x,
          self.y + other.y,
          self.z + other.z
        )
    }
}

impl ops::AddAssign for Vec3 {
    fn add_assign(&mut self, other: Vec3) {
        *self = vec3!(
          self.x + other.x,
          self.y + other.y,
          self.z + other.z
        )
    }
}

impl ops::Sub for Vec3{
    type Output = Vec3;
    fn sub(self,other:Vec3)->Vec3{
        vec3!(
          self.x - other.x,
          self.y - other.y,
          self.z - other.z
        )
    }
}

impl ops::SubAssign for Vec3 {
    fn sub_assign(&mut self, other: Vec3) {
        *self = vec3!(
          self.x - other.x,
          self.y - other.y,
          self.z - other.z
        )
    }
}
impl ops::Mul<Vec3> for Vec3{
  type Output = Vec3;
  fn mul(self,other:Vec3)->Vec3{
      vec3!(
         self.x * other.x,
         self.y * other.y,
         self.z * other.z
      )
  }
}

impl ops::MulAssign<Vec3> for Vec3 {
  fn mul_assign(&mut self, other: Vec3) {
      *self = vec3!(
         self.x * other.x,
         self.y * other.y,
         self.z * other.z
      )
  }
}

impl ops::Mul<f32> for Vec3{
    type Output = Vec3;
    fn mul(self,other:f32)->Vec3{
        vec3!(
           self.x * other,
           self.y * other,
           self.z * other
        )
    }
}

impl ops::MulAssign<f32> for Vec3 {
    fn mul_assign(&mut self, other: f32) {
        *self = vec3!(
           self.x * other,
           self.y * other,
           self.z * other
        )
    }
}

impl ops::Div<f32> for Vec3{
    type Output = Vec3;
    fn div(self,other:f32)->Vec3{
        vec3!(
          self.x / other,
          self.y / other,
          self.z / other
        )
    }
}

impl ops::DivAssign<f32> for Vec3 {
    fn div_assign(&mut self, other: f32) {
        *self = vec3!(
          self.x / other,
          self.y / other,
          self.z / other
        )
    }
}

pub fn cross(a:&Vec3,b:&Vec3)->Vec3{
  vec3!(
    a.y * b.z - a.z * b.y,
    a.z * b.x - a.x * b.z,
    a.x * b.y - a.y * b.x
  )
}

pub fn dot(a:&Vec3,b:&Vec3)->f32{
   a.x * b.x + a.y * b.y + a.z * b.z
}

impl Vec3{ 
  
  pub fn length(self)->f32{
    (self.x * self.x + self.y * self.y + self.z * self.z).sqrt()
  }
  pub fn normalize(&mut self){
    let l=self.length();
    if l!=0.0 {
      self.x/=l;
      self.y/=l;
      self.z/=l;
    } 
  }
  pub fn normalized(self)->Vec3{
    let l=self.length();
    if l!=0.0 {
      vec3!(self.x/l,self.y/l,self.z/l)
    }else{
      self
    }
  }
  pub fn project(self, v:&Vec3) ->Vec3{
    let b :f32= v.x * v.x + v.y * v.y + v.z * v.z;
    if b != 0.0 {
      let d :f32= (self.x * v.x + self.y * v.y + self.z * v.z) / b;
      vec3!(v.x * d, v.y * d, v.z * d)
    } else {
      vec3!(0.0,0.0,0.0)
    }
  }
  pub fn mult(self,m:&Matrix)->Vec3{
    vec3!(
      self.x *m.0 + self.y * m.4 + self.z * m.8 ,
      self.x *m.1 + self.y * m.5 + self.z * m.9 ,
      self.x *m.2 + self.y * m.6 + self.z * m.10
      )
  }
}