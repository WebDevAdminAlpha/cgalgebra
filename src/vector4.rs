use std::ops;
use std::fmt;
use std::f32;
use matrix::Matrix;
use serde_derive::{Serialize,Deserialize};

#[derive(Copy, Clone,Debug,Serialize,Deserialize)]
pub struct Vec4{
  pub x:f32,
  pub y:f32,
  pub z:f32,
  pub w:f32
}
#[macro_export]
macro_rules! vec4 {
  ($x:expr,$y:expr,$z:expr,$w:expr) => {
    Vec4{x:$x,y:$y,z:$z,w:$w}
 };
}
impl fmt::Display for Vec4 {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "x: {}, y: {}, z: {}, w: {}", self.x, self.y, self.z, self.w)
    }
}

impl ops::Add for Vec4{
    type Output = Vec4;
    fn add(self,other:Vec4)->Vec4{
        vec4!(
          self.x + other.x,
          self.y + other.y,
          self.z + other.z,
          self.w + other.w
        )
    }
}

impl ops::AddAssign for Vec4 {
    fn add_assign(&mut self, other: Vec4) {
        *self = vec4!(
            self.x + other.x,
            self.y + other.y,
            self.z + other.z,
            self.w + other.w
        )
    }
}

impl ops::Sub for Vec4{
    type Output = Vec4;
    fn sub(self,other:Vec4)->Vec4{
        vec4!(
          self.x - other.x,
          self.y - other.y,
          self.z - other.z,
          self.w - other.w
        )
    }
}

impl ops::SubAssign for Vec4 {
    fn sub_assign(&mut self, other: Vec4) {
        *self = vec4!(
          self.x - other.x,
          self.y - other.y,
          self.z - other.z,
          self.w - other.w
        )
    }
}

impl ops::Mul<f32> for Vec4{
    type Output = Vec4;
    fn mul(self,other:f32)->Vec4{
        vec4!(
          self.x * other,
          self.y * other,
          self.z * other,
          self.w * other
        )
    }
}

impl ops::MulAssign<f32> for Vec4 {
    fn mul_assign(&mut self, other: f32) {
        *self = vec4!(
          self.x * other,
          self.y * other,
          self.z * other,
          self.w * other
        )
    }
}

impl ops::Div<f32> for Vec4{
    type Output = Vec4;
    fn div(self,other:f32)->Vec4{
        vec4!(
          self.x / other,
          self.y / other,
          self.z / other,
          self.w / other
        )
    }
}

impl ops::DivAssign<f32> for Vec4 {
    fn div_assign(&mut self, other: f32) {
        *self = vec4!(
          self.x / other,
          self.y / other,
          self.z / other,
          self.w / other
        )
    }
}

pub fn cross(a:&Vec4,b:&Vec4)->Vec4{
  vec4!(
    a.y * b.z - a.z * b.y,
    a.z * b.x - a.x * b.z,
    a.x * b.y - a.y * b.x,
    1.0
  )
}

pub fn dot(a:&Vec4,b:&Vec4)->f32{
   a.x * b.x + a.y * b.y + a.z * b.z
}


impl Vec4{
  pub fn length(self)->f32{
    (self.x * self.x + self.y * self.y + self.z * self.z).sqrt()
  }
  pub fn normalize(&mut self){
    let l=self.length();
    if l!=0.0 {
      self.x/=l;
      self.y/=l;
      self.z/=l;
    } 
  }
  pub fn normalized(self)->Vec4{
    let l=self.length();
    if l!=0.0 {
      vec4!(self.x/l,self.y/l,self.z/l,1.0)
    }else{
      self
    }
  }
  pub fn project(self, v:&Vec4) ->Vec4{
    let b :f32= v.x * v.x + v.y * v.y + v.z * v.z;
    if b != 0.0 {
      let d :f32= (self.x * v.x + self.y * v.y + self.z * v.z) / b;
      vec4!(v.x * d, v.y * d, v.z * d,1.0)
    } else {
      vec4!(0.0,0.0,0.0,1.0)
    }
  }
  pub fn mult(self,m:&Matrix)->Vec4{
    vec4!(
      self.x *m.0 + self.y * m.4 + self.z * m.8 +self.w*m.12,
      self.x *m.1 + self.y * m.5 + self.z * m.9 +self.w*m.13,
      self.x *m.2 + self.y * m.6 + self.z * m.10+self.w*m.14,
      self.x *m.3 + self.y * m.7 + self.z * m.11+self.w*m.15
      )
  }
  pub fn mult2(&mut self,m:&Matrix){
   
    let  Vec4 {x,y,z,w} = *self;

    self.x=x *m.0 + y * m.4 + z * m.8 +w*m.12;
    self.y=x *m.1 + y * m.5 + z * m.9 +w*m.13;
    self.z=x *m.2 + y * m.6 + z * m.10+w*m.14;
    self.w=x *m.3 + y * m.7 + z * m.11+w*m.15;
  }
}