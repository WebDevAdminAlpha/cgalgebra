use std::ops;
use std::fmt;
use std::f32;
use matrix::Matrix;
use vector3::Vec3;
use vector4::Vec4;
use serde_derive::{Serialize,Deserialize};

#[derive(Copy, Clone,Debug,Serialize,Deserialize)]
pub struct Quaternion{
  pub x:f32,
  pub y:f32,
  pub z:f32,
  pub w:f32
}
#[macro_export]
macro_rules! quaternion {
  ($x:expr,$y:expr,$z:expr,$w:expr) => {
    Quaternion{x:$x,y:$y,z:$z,w:$w}
 };
}
impl fmt::Display for Quaternion {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "x: {}, y: {}, z: {}, w: {}", self.x, self.y, self.z, self.w)
    }
}

impl ops::Mul for Quaternion{
  type Output = Quaternion;
  fn mul( self, other: Quaternion) -> Quaternion {
    quaternion!(
      self.w * other.x + self.x * other.w + self.y * other.z - self.z * other.y,
      self.w * other.y + self.y * other.w + self.z * other.x - self.x * other.z,
      self.w * other.z + self.z * other.w + self.x * other.y - self.y * other.x,
      self.w * other.w - self.x * other.x - self.y * other.y - self.z * other.z
    )
  }
}

impl ops::MulAssign for Quaternion{
  fn mul_assign( &mut self, other: Quaternion) {    
    let x= self.w * other.x + self.x * other.w + self.y * other.z - self.z * other.y;
    let y= self.w * other.y + self.y * other.w + self.z * other.x - self.x * other.z;
    let z= self.w * other.z + self.z * other.w + self.x * other.y - self.y * other.x;
    let w= self.w * other.w - self.x * other.x - self.y * other.y - self.z * other.z;
    self.x=x;
    self.y=y;
    self.z=z;
    self.w=w;
  }
}

impl Quaternion{
  pub fn identity(&mut self) {
    self.x = 0.0;
    self.y = 0.0;
    self.z = 0.0;
    self.w = 1.0;
  }  
  pub fn conjugate(&mut self) {    
    self.x = -self.x;
    self.y = -self.y;
    self.z = -self.z;
  }  
  pub fn magnitude(self)->f32{
    (self.x * self.x + self.y * self.y + self.z * self.z+ self.w * self.w).sqrt()
  }
  pub fn normalize(&mut self){
    let l=self.magnitude();
    if l!=0.0 {
      self.x/=l;
      self.y/=l;
      self.z/=l;
      self.w/=l;
    } 
  }
  pub fn normalized(self)->Quaternion{
    let l=self.magnitude();
    if l!=0.0 {
      quaternion!(self.x/l,self.y/l,self.z/l,1.0)
    }else{
      self
    }
  }
  pub fn to_matrix(self) -> Matrix {
    let Quaternion{x,y,z,w}=self;
    let nq = x * x + y * y + z * z + w * w;
    let s = if nq > 0.0 { 2.0 / nq }else{ 0.0 };
    let xs = x * s;
    let ys = y * s;
    let zs = z * s;
    let wx = w * xs;
    let wy = w * ys;
    let wz = w * zs;
    let xx = x * xs;
    let xy = x * ys;
    let xz = x * zs;
    let yy = y * ys;
    let yz = y * zs;
    let zz = z * zs;
  

    Matrix(
      1.0 - (yy + zz),
      xy + wz,
      xz - wy,
      0.0,
      xy - wz,
      1.0 - (xx + zz),
      yz + wx,
      0.0,
      xz + wy,
      yz - wx,
      1.0 - (xx + yy),
      0.0,
      0.0,
      0.0,
      0.0,
      1.0
    )
  }
  pub fn from2vec4(&mut self,a:&Vec4, b:&Vec4) {
    self.x = a.y * b.z - a.z * b.y;
    self.y = a.z * b.x - a.x * b.z;
    self.z = a.x * b.y - a.y * b.x;
    self.w = a.x * b.x + a.y * b.y + a.z * b.z;
    self.w+=self.magnitude();
  }
  pub fn from2vec3(&mut self,a:&Vec3, b:&Vec3) {
    self.x = a.y * b.z - a.z * b.y;
    self.y = a.z * b.x - a.x * b.z;
    self.z = a.x * b.y - a.y * b.x;
    self.w = a.x * b.x + a.y * b.y + a.z * b.z ;
    self.w+=self.magnitude();
  }
  
  pub fn from_vec4_angle(&mut self,v:&Vec4,angle:f32) {    
    let sin = (angle * 0.5).sin();
    self.x = v.x * sin;
    self.y = v.y * sin;
    self.z = v.z * sin;
    self.w = (angle * 0.5).cos();
    self.normalize();
  }
  pub fn from_vect3_angle(&mut self,v:&Vec3,angle:f32) {    
    let sin = (angle * 0.5).sin();
    self.x = v.x * sin;
    self.y = v.y * sin;
    self.z = v.z * sin;
    self.w = (angle * 0.5).cos();
    self.normalize();
  }
  pub fn to_vector_angle(self)-> (Vec3,f32) {
      let q1=if self.w > 1.0 {
        self.normalized()
      }else{
        self
      };  
      let angle = 2.0 * q1.w.acos();
      let s = (1.0-q1.w*q1.w).sqrt();
      let v=if s!=0.0 {
        vec3!(q1.x / s,q1.y / s,q1.z / s)
      }else{
        vec3!(q1.x,q1.y,q1.z)
      };
      (v,angle)
    }
  pub fn slerp(&mut self,qa:&Quaternion, qb:&Quaternion, t:f32) {
    let cos_half_theta = qa.w * qb.w + qa.x * qb.x + qa.y * qb.y + qa.z * qb.z;
  
    if cos_half_theta.abs() >= 1.0 {
      self.w = qa.w;
      self.x = qa.x;
      self.y = qa.y;
      self.z = qa.z;      
    }else{    
      let  half_theta = cos_half_theta.acos();
      let  sin_half_theta = (1.0 - cos_half_theta*cos_half_theta).sqrt();
      if sin_half_theta.abs() < 0.001{
        self.w = qa.w * 0.5 + qb.w * 0.5;
        self.x = qa.x * 0.5 + qb.x * 0.5;
        self.y = qa.y * 0.5 + qb.y * 0.5;
        self.z = qa.z * 0.5 + qb.z * 0.5;
      }else{
        let ratio_a = ((1.0 - t) * half_theta).sin() / sin_half_theta;
        let ratio_b = (t * half_theta).sin() / sin_half_theta;
        self.w = qa.w * ratio_a + qb.w * ratio_b;
        self.x = qa.x * ratio_a + qb.x * ratio_b;
        self.y = qa.y * ratio_a + qb.y * ratio_b;
        self.z = qa.z * ratio_a + qb.z * ratio_b;
      }
    }
  }
}