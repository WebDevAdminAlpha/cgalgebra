use std::ops;
use std::fmt;
use std::f32;
use std::mem;
use vector3::Vec3;
use vector4::Vec4;
use serde_derive::{Serialize,Deserialize};


#[derive(Copy, Clone,Debug,Serialize,Deserialize)]
pub struct Matrix( //column-major
  pub f32,pub f32,pub f32,pub f32,//col1
  pub f32,pub f32,pub f32,pub f32,//col2
  pub f32,pub f32,pub f32,pub f32,//col3
  pub f32,pub f32,pub f32,pub f32,//col4
);

impl fmt::Display for Matrix {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "\n|{}, {}, {}, {}|\n|{}, {}, {}, {}|\n|{}, {}, {}, {}|\n|{}, {}, {}, {}|",
         self.0, self.4, self.8 , self.12,
         self.1, self.5, self.9 , self.13,
         self.2, self.6, self.10, self.14,
         self.3, self.7, self.11, self.15)
    }
}
pub fn new(m00:f32,m04:f32,m08:f32,m12:f32,
           m01:f32,m05:f32,m09:f32,m13:f32,
           m02:f32,m06:f32,m10:f32,m14:f32,
           m03:f32,m07:f32,m11:f32,m15:f32)->Matrix{
  Matrix(
    m00,m01,m02,m03,
    m04,m05,m06,m07,
    m08,m09,m10,m11,
    m12,m13,m14,m15
  )
}
pub fn identity()->Matrix{
  Matrix(
    1.0,0.0,0.0,0.0,
    0.0,1.0,0.0,0.0,
    0.0,0.0,1.0,0.0,
    0.0,0.0,0.0,1.0,
  )
}

pub fn translation(tx:f32,ty:f32,tz:f32)->Matrix{
  Matrix(
    1.0,0.0,0.0,tx,
    0.0,1.0,0.0,ty,
    0.0,0.0,1.0,tz,
    0.0,0.0,0.0,1.0,
  )
}

pub fn scale(sx:f32,sy:f32,sz:f32)->Matrix{
  Matrix(
    sx ,0.0,0.0,0.0,
    0.0,sy ,0.0,0.0,
    0.0,0.0,sz ,0.0,
    0.0,0.0,0.0,1.0,
  )
}
pub fn rotate_x(theta:f32)->Matrix{
  let sin=theta.sin();
  let cos=theta.cos();
  Matrix(
    1.0, 0.0,0.0,0.0,
    0.0, cos,sin,0.0,
    0.0,-sin,cos,0.0,
    0.0, 0.0,0.0,1.0,
  )
}
pub fn rotate_y(theta:f32)->Matrix{
  let sin=theta.sin();
  let cos=theta.cos();
  Matrix(
     cos,0.0,sin,0.0,
     0.0,1.0,0.0,0.0,
    -sin,0.0,cos,0.0,
     0.0,0.0,0.0,1.0,
  )
}

pub fn rotate_z(theta:f32)->Matrix{
  let sin=theta.sin();
  let cos=theta.cos();
  Matrix(
     cos,sin,0.0,0.0,
    -sin,cos,0.0,0.0,
     0.0,0.0,1.0,0.0,
     0.0,0.0,0.0,1.0,
  )
}

pub fn transposed(m:Matrix)->Matrix{
  Matrix(
    m.0, m.4, m.8 , m.12,
    m.1, m.5, m.9 , m.13,
    m.2, m.6, m.10, m.14,
    m.3, m.7, m.11, m.15
  )
}

impl Matrix{

  pub fn transpose(&mut self){
    mem::swap(&mut self.1, &mut self.4);
    mem::swap(&mut self.2, &mut self.8);
    mem::swap(&mut self.3, &mut self.12);
    mem::swap(&mut self.6, &mut self.9);
    mem::swap(&mut self.7, &mut self.13);
    mem::swap(&mut self.11, &mut self.14);
  }
}
impl ops::Mul<Vec3> for Matrix{
  type Output = Vec3;
  fn mul(self,v:Vec3)->Vec3{
    vec3!(v.x*self.0 + v.y*self.4 + v.z*self.8  + self.4,
		      v.x*self.1 + v.y*self.5 + v.z*self.9  + self.8,
          v.x*self.2 + v.y*self.6 + v.z*self.10 + self.12
    )
  }
}
impl ops::Mul<Vec4> for Matrix{
  type Output = Vec4;
  fn mul(self,v:Vec4)->Vec4{
    vec4!(v.x*self.0 + v.y*self.4 + v.z*self.8  + v.w *self.4,
		      v.x*self.1 + v.y*self.5 + v.z*self.9  + v.w *self.8,
          v.x*self.2 + v.y*self.6 + v.z*self.10 + v.w *self.12,
          v.x*self.3 + v.y*self.7 + v.z*self.11 + v.w *self.13
    )
  }
}
impl ops::Mul<Matrix> for Matrix{
    type Output = Matrix;
    fn mul(self,other:Matrix)->Matrix{
//                                   other.0, other.4, other.8 , other.12
//                                   other.1, other.5, other.9 , other.13
//                                   other.2, other.6, other.10, other.14
//                                   other.3, other.7, other.11, other.15
//self.0, self.4, self.8 , self.12   res.0  , res.4  , res.8   , res.12
//self.1, self.5, self.9 , self.13   res.1  , res.5  , res.9   , res.13
//self.2, self.6, self.10, self.14   res.2  , res.6  , res.10  , res.14
//self.3, self.7, self.11, self.15   res.3  , res.7  , res.11  , res.15

        Matrix(
          self.0*other.0+ self.4*other.1+ self.8 *other.2+ self.12*other.3,
          self.1*other.0+ self.5*other.1+ self.9 *other.2+ self.13*other.3,
          self.2*other.0+ self.6*other.1+ self.10*other.2+ self.14*other.3,
          self.3*other.0+ self.7*other.1+ self.11*other.2+ self.15*other.3,

          self.0*other.4+ self.4*other.5+ self.8 *other.6+ self.12*other.7,
          self.1*other.4+ self.5*other.5+ self.9 *other.6+ self.13*other.7,
          self.2*other.4+ self.6*other.5+ self.10*other.6+ self.14*other.7,
          self.3*other.4+ self.7*other.5+ self.11*other.6+ self.15*other.7,

          self.0*other.8+ self.4*other.9+ self.8 *other.10+ self.12*other.11,
          self.1*other.8+ self.5*other.9+ self.9 *other.10+ self.13*other.11,
          self.2*other.8+ self.6*other.9+ self.10*other.10+ self.14*other.11,
          self.3*other.8+ self.7*other.9+ self.11*other.10+ self.15*other.11,

          self.0*other.12+ self.4*other.13+ self.8 *other.14+ self.12*other.15,
          self.1*other.12+ self.5*other.13+ self.9 *other.14+ self.13*other.15,
          self.2*other.12+ self.6*other.13+ self.10*other.14+ self.14*other.15,
          self.3*other.12+ self.7*other.13+ self.11*other.14+ self.15*other.15,
        )
    }
}